# PyPI Mirror for CI

This project builds a Python package index in Gitlab that can be used in our CI system to accomodate for runners that only have the dune-project domain white-listed.

## How to use the mirror in CI

Add the following snippet to your `.gitlab-ci.yml`:

```
before_script:
  - echo 'CMAKE_FLAGS+="-DDUNE_PYTHON_ADDITIONAL_PIP_PARAMS=--extra-index-url=https://gitlab.dune-project.org/api/v4/projects/1007/packages/pypi/simple"' >> /duneci/cmake-flags/pypi_mirror
```

## How to update the index

* Set up pyenv according to [this tutorial](https://realpython.com/intro-to-pyenv/)
* Make sure that you have the following versions of Python installed with pyenv: `3.6.12`, `3.7.9`, `3.8.7`, `3.9.1`
* Clone this repo
* Go to Gitlab Profile/Setting/Access Tokens and create a personal access token with at least the `write_registry` scope.
* Run `TWINE_PASSWORD=<token> ./rebuild-dune-index.sh`

## Couldn't we update the mirror in CI?

Sure, we can. But then it requires a dedicated runner with suitable permissions, because otherwise it is running into the problem that it tries to solve.
